﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Что такое GWSET?", 0, 0)
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Установка GWSET")
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Настройка GWSET")
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Как работает GWSET?")
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Menu1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.СкачатьGWSETToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ОткрытьРедакторВызоваGWSETToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreeImages = New System.Windows.Forms.ImageList(Me.components)
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.PBT = New System.Windows.Forms.Timer(Me.components)
        Me.SaveFile = New System.Windows.Forms.SaveFileDialog()
        Me.Menu1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TreeView1.ContextMenuStrip = Me.Menu1
        Me.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.TreeView1.FullRowSelect = True
        Me.TreeView1.ImageIndex = 0
        Me.TreeView1.ImageList = Me.TreeImages
        Me.TreeView1.Location = New System.Drawing.Point(0, 0)
        Me.TreeView1.Name = "TreeView1"
        TreeNode5.ImageIndex = 0
        TreeNode5.Name = "GWSETWhatIs"
        TreeNode5.SelectedImageIndex = 0
        TreeNode5.Text = "Что такое GWSET?"
        TreeNode6.Name = "GWSETSetup"
        TreeNode6.Text = "Установка GWSET"
        TreeNode7.Name = "GWSETSettings"
        TreeNode7.Text = "Настройка GWSET"
        TreeNode8.Name = "GWSETJob"
        TreeNode8.StateImageKey = "(отсутствует)"
        TreeNode8.Text = "Как работает GWSET?"
        Me.TreeView1.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode5, TreeNode6, TreeNode7, TreeNode8})
        Me.TreeView1.SelectedImageIndex = 0
        Me.TreeView1.ShowLines = False
        Me.TreeView1.ShowPlusMinus = False
        Me.TreeView1.ShowRootLines = False
        Me.TreeView1.Size = New System.Drawing.Size(132, 362)
        Me.TreeView1.TabIndex = 0
        '
        'Menu1
        '
        Me.Menu1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Menu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.СкачатьGWSETToolStripMenuItem, Me.ОткрытьРедакторВызоваGWSETToolStripMenuItem})
        Me.Menu1.Name = "Menu1"
        Me.Menu1.ShowImageMargin = False
        Me.Menu1.Size = New System.Drawing.Size(354, 48)
        '
        'СкачатьGWSETToolStripMenuItem
        '
        Me.СкачатьGWSETToolStripMenuItem.Name = "СкачатьGWSETToolStripMenuItem"
        Me.СкачатьGWSETToolStripMenuItem.Size = New System.Drawing.Size(353, 22)
        Me.СкачатьGWSETToolStripMenuItem.Text = "Скачать GWSET"
        '
        'ОткрытьРедакторВызоваGWSETToolStripMenuItem
        '
        Me.ОткрытьРедакторВызоваGWSETToolStripMenuItem.Name = "ОткрытьРедакторВызоваGWSETToolStripMenuItem"
        Me.ОткрытьРедакторВызоваGWSETToolStripMenuItem.Size = New System.Drawing.Size(353, 22)
        Me.ОткрытьРедакторВызоваGWSETToolStripMenuItem.Text = "Открыть редактор обработки результатов GWSET"
        '
        'TreeImages
        '
        Me.TreeImages.ImageStream = CType(resources.GetObject("TreeImages.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.TreeImages.TransparentColor = System.Drawing.Color.Transparent
        Me.TreeImages.Images.SetKeyName(0, "information.png")
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TreeView1)
        Me.SplitContainer1.Panel1MinSize = 132
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel2MinSize = 140
        Me.SplitContainer1.Size = New System.Drawing.Size(484, 362)
        Me.SplitContainer1.SplitterDistance = 132
        Me.SplitContainer1.TabIndex = 1
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox1.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(0, 0)
        Me.TextBox1.MaxLength = 100000
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox1.Size = New System.Drawing.Size(348, 362)
        Me.TextBox1.TabIndex = 0
        '
        'PBT
        '
        Me.PBT.Interval = 1000
        '
        'SaveFile
        '
        Me.SaveFile.Filter = "GWSET|*.exe"
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(484, 362)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(300, 93)
        Me.Name = "Main"
        Me.Text = "Справка по GWSET"
        Me.Menu1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TreeImages As System.Windows.Forms.ImageList
    Friend WithEvents Menu1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents СкачатьGWSETToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ОткрытьРедакторВызоваGWSETToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PBT As System.Windows.Forms.Timer
    Friend WithEvents SaveFile As System.Windows.Forms.SaveFileDialog
End Class
