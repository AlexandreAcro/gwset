﻿Public Class Main
    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Dim lines(0) As String
        If TreeView1.SelectedNode.Index = 0 Then
            Array.Resize(lines, 6)
            lines(0) = "    Что такое GWSET?"
            lines(1) = "  GWSET - это плагин для Windows, который позволяет пользователю получить доступ к полному управлению системой компьютера."
            lines(2) = "Вызывается плагин через командную строку или функцию 'Выполнить' по своему имени: gwset."
            lines(3) = "Сам плагин был придуман в начале 2000 годов, когда появился Windows 2000 (самая первая версия плагина была сделана под него). Ранее, в силу недоработанности Windows, плагин имел скудные возможности, но с появлением Windows ХР плагин стал мощнейшим инструментом работы с Windows. Кстати, плагин уже появился и на новейшую версию Windows 10."
            lines(4) = "  GWSET ничего сам не делает, он использует компоненты, которыми Windows пользуется сам, чтобы что-либо делать. Таким образом получается, что GWSET имеет права, доступа к тем системным данным и программам, к которым не вся система имеет доступ. Основных компонентов для плагина - 4. Это те компоненты, с помощью которых система сама осуществляет большое колличество операций. Также плагин использует ещё 2 компонента - это компоненты плагинного и пользовательского уровня, из-за этого его часто называют 'Консоль Управления Плагинами', хотя управление плагинами это достаточно малая часть всего функционала плагина."
            lines(5) = "-- Установка см. в разделе 'Установка GWSET' --"
            TextBox1.Lines = lines
        ElseIf TreeView1.SelectedNode.Index = 1 Then
            Array.Resize(lines, 4)
            lines(0) = "    Установка GWSET"
            lines(1) = "  Вы можете скачать GWSET, нажав правую кнопку на справочном списке, а затем нажать кнопку 'Скачать GWSET' и выбрать папку для сохранения."
            lines(2) = "  Для установки - поместите скачанный файл в папку C:\Windows. Всё!"
            lines(3) = "-- Настройка см. в разделе 'Настройка GWSET' --"
            TextBox1.Lines = lines
        ElseIf TreeView1.SelectedNode.Index = 2 Then
            Array.Resize(lines, 3)
            lines(0) = "    Как работает GWSET?"
            lines(1) = "  Настройка плагина производится через раздел реестра 'HKEY_CURRENT_USER\Plugins\GWSET'. В этом разделе находятся настройки GWSET: Active, EnterCommandResults и BlackList. Active отвечает за включение/выключение плагина т.е. при значении ActivateGWSET=true плагин работает, а при значении ActivateGWSET=false - нет. BlackList - это черный список команд. Знак / в начале убирать нельзя, а писать надо на следующей строке. EnterCommandResults - этот параметр вручную лучше не менять, так как он отвечает за обработку и вывод результатов исполнения команды. В это поле вводится плагинный код, написание которого возможно в программе WriterPlugins, хотя если вы хорошо знаете язык WriterPlugins, то сможете написать код и без него. Вы можете отредактировать этот код нажав правой кнопкой мыши на справочном списке, а затем нажать кнопку 'Открыть редактор обработки результатов GWSET'."
            lines(2) = "-- Использование см. в разделе 'Как работает GWSET?' --"
            TextBox1.Lines = lines
        ElseIf TreeView1.SelectedNode.Index = 3 Then
            Array.Resize(lines, 3)
            lines(0) = "    Как работает GWSET?"
            lines(1) = "  После установки и запуска GWSET вы увидите окно плагина. В нижнем поле ввода вводятся команды, которые при исполнении передаются в исполнительные компоненты (см. 'Что такое GWSET?'). Однако, команды перед передачей компонентам обрабатываются непосредственно плагином. Команда для плагина будет выглядеть так: \<команда> или \<шаблон>\<команда>. Пример: \system get date set C:\Windows\notepad.exe или \Admins\date set C:\Windows\notepad.exe - установит дату открытия для Блокнота, а если писать команду напрямую для компонентов, то она будет выглядеть так: \\<команда>. Пример: \\open clock time date send set file C:\Windows\notepad.exe - установит дату открытия для Блокнота. Списка команд у плагина нет, так как этот список напрямую зависит от установленных программ, системы, плагинов и многого другого."
            lines(2) = "  Верхнее поле ввода имеет больше функций, чем нижнее, что не мешает работать ему и как оно. В верхнее поле ввода можно ввести комманду также, как и в нижнее: \system get date set C:\Windows\notepad.exe или \Admins\date set C:\Windows\notepad.exe, или \\open clock time date send set file C:\Windows\notepad.exe, но следует заметить, что в это поле можно вводить несколько команд одновременно, писать туда код WriterPlugins или работать с параметрами. Самая верхняя строка этого поля - это строка функций. Здесь задаётся режим работы этого поля. Пример: /command 1 2 - выполнит команду, находящуюся на строках ниже, т.е. команды на строках 1 и 2. О выполнении нескольких команд поговорили, теперь поговорим о коде. Для его выполнения необходимо вписать на строке функций: /WriterPlugins kod - тогда при исполнении команды будет запущен целый плагин, который будет исполнятся, пока не отключится или не будет закрыт. Параметры - очень полезная вещь! Для их изменения порой приходится писать длинные команды, а тут достаточно просто изменить значение на новое. Пример: чтобы командой изменить парамерт приоритета загрузки программ надо писать команду /system user all load priority set programm <программа>, а параметром PriorityLoad=<программа>."
            TextBox1.Lines = lines
        End If
    End Sub
    Private Sub Main_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Start.Close()
    End Sub
    Private Sub СкачатьGWSETToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles СкачатьGWSETToolStripMenuItem.Click
        SaveFile.FileName = "gwset"
        Dim result As DialogResult = SaveFile.ShowDialog
        If result <> DialogResult.OK Then
            Exit Sub
        End If
        TreeView1.Enabled = False
        TextBox1.Text = "Скачивание: 0%"
        loading = 0
        PBT.Interval = Start.network_speed * 50
        PBT.Start()
    End Sub
    Dim loading As Short = 0
    Private Sub PBT_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PBT.Tick
        If My.Computer.Network.IsAvailable = False Then
            PBT.Stop()
            Dim result As MsgBoxResult = MsgBox("Нет соединения с интернетом!", MsgBoxStyle.RetryCancel, "Ошибка")
            If result = MsgBoxResult.Retry Then
                PBT.Start()
            Else
                Me.Close()
            End If
        ElseIf loading = 100 Then
            PBT.Stop()
            TextBox1.Text = "Скачивание: 100%"
            My.Computer.FileSystem.WriteAllBytes(SaveFile.FileName, My.Resources.GWSET, True)
            Dim keys() As String = My.Computer.Registry.CurrentUser.GetSubKeyNames, exits As Boolean = False
            For Each i As String In keys
                If i = "Plugins" Then
                    exits = True
                End If
            Next
            If exits = False Then
                My.Computer.Registry.CurrentUser.CreateSubKey("Plugins")
            End If
            exits = False
            For Each i As String In My.Computer.Registry.CurrentUser.OpenSubKey("Plugins", False).GetSubKeyNames
                If i = "GWSET" Then
                    exits = True
                End If
            Next
            If exits = False Then
                My.Computer.Registry.CurrentUser.CreateSubKey("Plugins\GWSET")
            End If
            Dim gwset_key As Microsoft.Win32.RegistryKey = My.Computer.Registry.CurrentUser.OpenSubKey("Plugins\GWSET", True)
            Dim launch(4) As String, blacklist(0) As String
            launch(0) = "DataProgram.Focus.>goto<"
            launch(1) = "Control.Plugining.Setup..EnterKey...PlushProgram.<goto>"
            launch(2) = "Aktivate.Control.Forwent..STab..>goto<"
            launch(3) = "Activated.OpenPassword..Reestr.Aktivate"
            launch(4) = "Control.KeyWindows.Key.Set.EnterProgram..<goto>"
            blacklist(0) = "/"
            gwset_key.SetValue("Active", "ActivateGWSET=true", Microsoft.Win32.RegistryValueKind.String)
            gwset_key.SetValue("EnterCommandResults", launch, Microsoft.Win32.RegistryValueKind.MultiString)
            gwset_key.SetValue("BlackList", blacklist, Microsoft.Win32.RegistryValueKind.MultiString)
            My.Computer.Registry.CurrentUser.Close()
            TreeView1.Enabled = True
            TreeView1.SelectedNode.Name = "GWSETSetup"
            Dim lines(3) As String
            lines(0) = "    Установка GWSET"
            lines(1) = "  Вы можете скачать GWSET, нажав правую кнопку на справочном списке, а затем нажать кнопку 'Скачать GWSET' и выбрать папку для сохранения."
            lines(2) = "  Для установки - поместите скачанный файл в папку C:\Windows. Всё!"
            lines(3) = "-- Настройка см. в разделе 'Настройка GWSET' --"
            TextBox1.Lines = lines
        Else
            loading += 1
            TextBox1.Text = "Скачивание: " & loading & "%"
        End If
    End Sub
    Private Sub ОткрытьРедакторВызоваGWSETToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ОткрытьРедакторВызоваGWSETToolStripMenuItem.Click
        Redactor.ShowDialog()
    End Sub
End Class