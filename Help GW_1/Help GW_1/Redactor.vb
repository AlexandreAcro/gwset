﻿Public Class Redactor
    Dim gwset_key As Microsoft.Win32.RegistryKey
    Private Sub Redactor_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Dim keys() As String = My.Computer.Registry.CurrentUser.GetSubKeyNames, exits As Boolean = False
        For Each i As String In keys
            If i = "Plugins" Then
                exits = True
            End If
        Next
        If exits = False Then
            My.Computer.Registry.CurrentUser.CreateSubKey("Plugins")
        End If
        exits = False
        For Each i As String In My.Computer.Registry.CurrentUser.OpenSubKey("Plugins", False).GetSubKeyNames
            If i = "GWSET" Then
                exits = True
            End If
        Next
        If exits = False Then
            My.Computer.Registry.CurrentUser.CreateSubKey("Plugins\GWSET")
        End If
        gwset_key = My.Computer.Registry.CurrentUser.OpenSubKey("Plugins\GWSET", True)
        If gwset_key.GetValue("EnterCommandResults", "").ToString = "" Then
            Dim launch(4) As String
            launch(0) = "DataProgram.Focus.>goto<"
            launch(1) = "Control.Plugining.Setup..EnterKey...PlushProgram.<goto>"
            launch(2) = "Aktivate.Control.Forwent..STab..>goto<"
            launch(3) = "Activated.OpenPassword..Reestr.Aktivate"
            launch(4) = "Control.KeyWindows.Key.Set.EnterProgram..<goto>"
            gwset_key.SetValue("EnterCommandResults", launch, Microsoft.Win32.RegistryValueKind.MultiString)
            My.Computer.Registry.CurrentUser.Close()
        End If
        TextBox2.Lines = gwset_key.GetValue("EnterCommandResults", "")
        TextBox1.Text = "Позиция:" & TextBox2.SelectionStart & "/" & TextBox2.Text.Length & " Выделение:" & TextBox2.SelectionStart + TextBox2.SelectionLength
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim launch(4) As String
        launch(0) = "DataProgram.Focus.>goto<"
        launch(1) = "Control.Plugining.Setup..EnterKey...PlushProgram.<goto>"
        launch(2) = "Aktivate.Control.Forwent..STab..>goto<"
        launch(3) = "Activated.OpenPassword..Reestr.Aktivate"
        launch(4) = "Control.KeyWindows.Key.Set.EnterProgram..<goto>"
        TextBox2.Lines = launch
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        gwset_key.SetValue("EnterCommandResults", TextBox2.Lines, Microsoft.Win32.RegistryValueKind.MultiString)
    End Sub
    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox2.KeyDown
        TextBox1.Text = "Позиция:" & TextBox2.SelectionStart & "/" & TextBox2.Text.Length & " " & "Выделение:" & TextBox2.SelectionStart + TextBox2.SelectionLength
    End Sub
    Private Sub TextBox2_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox2.MouseMove
        TextBox1.Text = "Позиция:" & TextBox2.SelectionStart & "/" & TextBox2.Text.Length & " " & "Выделение:" & TextBox2.SelectionStart + TextBox2.SelectionLength
    End Sub
    Private Sub TextBox2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        TextBox1.Text = "Позиция:" & TextBox2.SelectionStart & "/" & TextBox2.Text.Length & " " & "Выделение:" & TextBox2.SelectionStart + TextBox2.SelectionLength
    End Sub
End Class
