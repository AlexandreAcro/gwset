﻿Public Class Start
    Dim mode As Short
    Public network_speed As Integer
    Private Sub PBT_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PBT.Tick
        If mode = 0 Then
            PBT.Stop()
            If My.Computer.Network.IsAvailable = True Then
                network_speed = Math.Round(Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces(0).Speed / 1048576)
                If network_speed > 95 Then network_speed = 95
                network_speed = 96 - network_speed
            Else
                network_speed = 96
            End If
            PBT.Interval = network_speed * 200
            mode = 1
            PBT.Start()
        ElseIf mode = 1 Then
            If My.Computer.Network.IsAvailable = False Then
                PBT.Stop()
                PictureBox1.Enabled = False
                Dim result As MsgBoxResult = MsgBox("没有连接到互联网！" & Chr(10) & "No internet connection!" & Chr(10) & "No hay conexión a Internet!" & Chr(10) & "لا يوجد اتصال بالإنترنت!" & Chr(10) & "Нет соединения с интернетом!" & Chr(10) & "Sem conexão com a Internet!" & Chr(10) & "Keine Verbindung mit dem Internet!" & Chr(10) & "Pas de connexion à Internet!", MsgBoxStyle.RetryCancel, "GWSET")
                If result = MsgBoxResult.Retry Then
                    PBT.Start()
                    PictureBox1.Enabled = True
                Else
                    Me.Close()
                End If
            ElseIf ProgressBar1.Value = 40 Then
                PBT.Stop()
                Label9.Text = "Язык: Русский"
                mode = 2
                PBT.Interval = 1000
                PictureBox1.Visible = False
                PictureBox2.Visible = True
                PBT.Start()
            Else
                ProgressBar1.Value += 4
            End If
        ElseIf mode = 2 Then
            Label1.Visible = False
            mode = 3
        ElseIf mode = 3 Then
            Label2.Visible = False
            mode = 4
        ElseIf mode = 4 Then
            Label3.Visible = False
            mode = 5
        ElseIf mode = 5 Then
            Label4.Visible = False
            mode = 6
        ElseIf mode = 6 Then
            Label6.Visible = False
            mode = 7
        ElseIf mode = 7 Then
            Label7.Visible = False
            mode = 8
        ElseIf mode = 8 Then
            Label8.Visible = False
            mode = 9
        ElseIf mode = 9 Then
            Label8.Text = "Загрузка данных: 0 mb / 40 mb"
            Label8.Visible = True
            Label5.Visible = False
            ProgressBar1.Value = 0
            PBT.Interval = network_speed * 400
            mode = 10
            PictureBox3.Visible = True
        ElseIf mode = 10 Then
            If My.Computer.Network.IsAvailable = False Then
                PBT.Stop()
                PictureBox2.Enabled = False
                Dim result As MsgBoxResult = MsgBox("Нет соединения с интернетом!", MsgBoxStyle.RetryCancel, "Ошибка")
                If result = MsgBoxResult.Retry Then
                    PBT.Start()
                    PictureBox2.Enabled = True
                Else
                    Me.Close()
                End If
            ElseIf ProgressBar1.Value = 40 Then
                PBT.Stop()
                mode = 11
                PBT.Interval = 1000
                PictureBox2.Visible = False
                PictureBox3.Visible = False
                Label8.Text = "Загрузка данных завершена."
                PBT.Start()
            Else
                ProgressBar1.Value += 1
                Label8.Text = "Загрузка данных: " & ProgressBar1.Value & " mb / 40 mb"
            End If
        ElseIf mode = 11 Then
            PBT.Stop()
            Main.Show()
            Me.Hide()
        End If
    End Sub
    Private Sub Form1_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        mode = 0
        PBT.Start()
    End Sub
End Class
