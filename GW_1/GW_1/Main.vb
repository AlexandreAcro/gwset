﻿Public Class Main
    Public end_dat As Boolean = True
    Dim mine_dat_data(7) As String
    Dim gwset_key As Microsoft.Win32.RegistryKey
    Dim blacklist() As String
    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        If Me.WindowState = 1 Then
            Mini.ShowDialog()
            If end_dat = True Then
                Me.Close()
            Else
                end_dat = True
                Me.Show()
                Me.WindowState = 0
            End If
        End If
    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim exits As Boolean = False
        For Each i As String In My.Computer.Registry.CurrentUser.GetSubKeyNames
            If i = "Plugins" Then
                exits = True
            End If
        Next
        If exits = False Then
            My.Computer.Registry.CurrentUser.CreateSubKey("Plugins")
        End If
        exits = False
        For Each i As String In My.Computer.Registry.CurrentUser.OpenSubKey("Plugins", False).GetSubKeyNames
            If i = "GWSET" Then
                exits = True
            End If
        Next
        If exits = False Then
            My.Computer.Registry.CurrentUser.CreateSubKey("Plugins\GWSET")
        End If
        gwset_key = My.Computer.Registry.CurrentUser.OpenSubKey("Plugins\GWSET", False)
        If gwset_key.GetValue("Active", "").ToString = "" Or gwset_key.GetValue("EnterCommandResults", "").ToString = "" Or gwset_key.GetValue("BlackList", "").ToString = "" Then
            Dim key As Microsoft.Win32.RegistryKey = My.Computer.Registry.CurrentUser.OpenSubKey("Plugins\GWSET", True)
            If key.GetValue("Active", "") = "" Then
                key.SetValue("Active", "ActivateGWSET=true", Microsoft.Win32.RegistryValueKind.String)
            End If
            If key.GetValue("EnterCommandResults", "").ToString = "" Then
                Dim l(4) As String
                l(0) = "DataProgram.Focus.>goto<"
                l(1) = "Control.Plugining.Setup..EnterKey...PlushProgram.<goto>"
                l(2) = "Aktivate.Control.Forwent..STab..>goto<"
                l(3) = "Activated.OpenPassword..Reestr.Aktivate"
                l(4) = "Control.KeyWindows.Key.Set.EnterProgram..<goto>"
                key.SetValue("EnterCommandResults", l, Microsoft.Win32.RegistryValueKind.MultiString)
            End If
            If key.GetValue("BlackList", "").ToString = "" Then
                Dim bl(0) As String
                bl(0) = "/"
                key.SetValue("BlackList", bl, Microsoft.Win32.RegistryValueKind.MultiString)
            End If
            My.Computer.Registry.CurrentUser.Close()
        End If
        If gwset_key.GetValue("Active", "ActivateGWSET=true").ToString = "ActivateGWSET=false" Then
            Me.Close()
        End If
        If gwset_key.GetValue("BlackList", "").ToString <> "" Then
            blacklist = gwset_key.GetValue("BlackList", "")
        End If
        If My.Computer.FileSystem.FileExists("C:\Windows\gwset.exe") = False Then
            MsgBox("Плагину GWSET не удаётся получить доступ к компонентам Windows!" & Chr(10) & "    Убедитесь, что плагин находится в папке C:\Windows и имеет имя gwset.", , "! Ошибка доступа")
            Me.Close()
        End If
        TextBox1.Text = My.User.Name
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        P_dat.Text = "Admins"
        Command_dat.Text = "/Admins/<setting; data; mode> <data; mode; statys> []"
        mine_dat_data(0) = "/Admins/Setting 6"
        mine_dat_data(1) = "Antivirus-Ativator="
        mine_dat_data(2) = "AutoSaver="
        mine_dat_data(3) = "MeGuard="
        mine_dat_data(4) = "AutoData="
        mine_dat_data(5) = "MainProcess="
        mine_dat_data(6) = "SaveProcessing="
        mine_dat_data(7) = "AdminsGuard="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        P_dat.Text = "Components"
        Command_dat.Text = "/Komponents/<data; mode> <mode> []"
        mine_dat_data(0) = "/Komponents/Setting 6"
        mine_dat_data(1) = "AllComponentsAktiv="
        mine_dat_data(2) = "AllComponentsGuard="
        mine_dat_data(3) = "AddComponent="
        mine_dat_data(4) = "DeleteComponent="
        mine_dat_data(5) = "AutoLoad="
        mine_dat_data(6) = "SleppingComponent="
        mine_dat_data(7) = "DisableComponent="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        P_dat.Text = "Plugins Load"
        Command_dat.Text = "/Plugins Load/<setting> <statys> []"
        mine_dat_data(0) = "/Plugins Load/Setting 6"
        mine_dat_data(1) = "LoadPlugins="
        mine_dat_data(2) = "GuardPlugins="
        mine_dat_data(3) = "GivePluginsSettingForWindows="
        mine_dat_data(4) = "AddPlugin="
        mine_dat_data(5) = "DeletePlugin="
        mine_dat_data(6) = "PriorityPlugin="
        mine_dat_data(7) = "UnPriorityPlugin="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        P_dat.Text = "Process"
        Command_dat.Text = "/Process/<setting; data; mode> <data; mode; statys> []"
        mine_dat_data(0) = "/Process/Setting 6"
        mine_dat_data(1) = "AddProcess="
        mine_dat_data(2) = "DeleteProcess="
        mine_dat_data(3) = "ControlProcess="
        mine_dat_data(4) = "DisableProcess="
        mine_dat_data(5) = "BanProcess="
        mine_dat_data(6) = "UnBanProcess="
        mine_dat_data(7) = "ResetProcess="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        P_dat.Text = "Sustem Load"
        Command_dat.Text = "/Sustem Load/<data> <mode; statys> []"
        mine_dat_data(0) = "/Sustem Load/Setting 6"
        mine_dat_data(1) = "AllComponents="
        mine_dat_data(2) = "Components="
        mine_dat_data(3) = "SustemComponents="
        mine_dat_data(4) = "UserComponents="
        mine_dat_data(5) = "AdminComponents="
        mine_dat_data(6) = "GuardComponents="
        mine_dat_data(7) = "DeleteComponents="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        P_dat.Text = "Sustem Guard"
        Command_dat.Text = "/Sustem Guard/<data> <statys> []"
        mine_dat_data(0) = "/Sustem Guard/Setting 6"
        mine_dat_data(1) = "SustemGuard="
        mine_dat_data(2) = "ComponentGuard="
        mine_dat_data(3) = "MeGuardActiv"
        mine_dat_data(4) = "MeGuardSettings="
        mine_dat_data(5) = "PluginsGuard="
        mine_dat_data(6) = "DeleteAllPlugins="
        mine_dat_data(7) = "DisableAllProcess="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        P_dat.Text = "Users Load"
        Command_dat.Text = "/Users Load/<data> <mode; statys> []"
        mine_dat_data(0) = "/Users Load/Setting 6"
        mine_dat_data(1) = "UserDataLoad="
        mine_dat_data(2) = "AdminDataLoad="
        mine_dat_data(3) = "LoadControlPannel="
        mine_dat_data(4) = "LoadSustemPannel="
        mine_dat_data(5) = "DeleteUser="
        mine_dat_data(6) = "CreateUser="
        mine_dat_data(7) = "ResetUser="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        P_dat.Text = "Users Guard"
        Command_dat.Text = "/Users Guard/<data> <statys> []"
        mine_dat_data(0) = "/Users Guard/Setting 6"
        mine_dat_data(1) = "UsersGuard="
        mine_dat_data(2) = "GuardAdminProcess="
        mine_dat_data(3) = "GuardUserProcess="
        mine_dat_data(4) = "DeleteUserProcess="
        mine_dat_data(5) = "AddUserProcess="
        mine_dat_data(6) = "GuardUserPlugins="
        mine_dat_data(7) = "DeleteUserPlugins="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        P_dat.Text = "Priority"
        Command_dat.Text = "/Priority/<data> <statys> []"
        mine_dat_data(0) = "/Priority/Setting 6"
        mine_dat_data(1) = "PriorityProgram="
        mine_dat_data(2) = "PriorityLoad="
        mine_dat_data(3) = "PriorityForAntivirus="
        mine_dat_data(4) = "PriorityLine="
        mine_dat_data(5) = "PriorityProcess="
        mine_dat_data(6) = "PriorityPlayer="
        mine_dat_data(7) = "PriorityDeinstallator="
        Main_dat.Lines = mine_dat_data
    End Sub
    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        For Each i As String In blacklist
            If Main_dat.Text.TrimStart("/") = i And i <> "/" Then
                MsgBox("Can not execute this command!", , "Error")
                Exit Sub
            End If
        Next
        For Each i As String In blacklist
            If Command_dat.Text.TrimStart("/") = i And i <> "/" Then
                MsgBox("Can not execute this command!", , "Error")
                Exit Sub
            End If
        Next
        If gwset_key.GetValue("EnterCommandResults", "").ToString <> "" Then
            DirectoryPrewiev.Close()
            OneOrTwoNumbers.Close()
            DirectoryPrewiev.Show()
            OneOrTwoNumbers.Show()
        End If
    End Sub
End Class
